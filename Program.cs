using Microsoft.EntityFrameworkCore;
using TaskApi.Context;
using TaskApi.ITaskRepository;
using TaskApi.TaskRepository;

namespace TaskApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            builder.Services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Version = "v1",
                    Title = "Dummy Web Api",
                    Description = "Its a dummy web api"
                });
            });

            // Add services to the container.

            builder.Services.AddControllers();



            // Add Required Dependencies here
            builder.Services.AddDbContext<TaskApiDbContext>(options =>
            {
                options.UseSqlServer(builder.Configuration.GetConnectionString("TaskApiDbConnection"));

            });
            builder.Services.AddTransient<ITask, TaskRepo>();
            builder.Services.AddTransient<ISubTask, SubTaskRepo>();
            var app = builder.Build();

            // Configure the HTTP request pipeline.



            app.UseSwagger();
            app.UseSwaggerUI();

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}