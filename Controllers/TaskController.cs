﻿using Microsoft.AspNetCore.Mvc;
using TaskApi.ITaskRepository;
using TaskApi.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TaskApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        // Add required dependencies here
        ITask _itask;
        public TaskController(ITask task)
        {
            _itask = task;

        }
        [HttpPost]

        public void Post(Tasks task)
        {
            _itask.Create(task);

        }
        [HttpGet("{id}")]
        public Tasks Get(int id) 
        {
            return _itask.GetTaskById(id);
        }
        [HttpGet]
        public List<Tasks> GetTasks() 
        {
          return _itask.GetTask();

        }
        [HttpDelete("{id}")]
        public IActionResult Delete(int id) 
        {
            if (_itask.GetTaskById(id) == null)     
         {  
               return NotFound("There is no record");

            }
            else
            {
                _itask.Delete(id);
                return Ok("Deleted");

            }
        }


    }
}
