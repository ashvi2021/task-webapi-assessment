﻿using System.Threading.Tasks;
using TaskApi.Context;
using TaskApi.ITaskRepository;
using TaskApi.Models;

namespace TaskApi.TaskRepository
{
    public class TaskRepo : ITask
    {
        TaskApiDbContext _db;
        public TaskRepo(TaskApiDbContext db)
        {
            _db = db;
        }
        public void Create(Tasks task)
        {
            _db.tasks.Add(task);
            _db.SaveChanges();
             //throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            Tasks task = _db.tasks.FirstOrDefault(x => x.Id == id);
            if (task != null) 
            {
                _db.tasks.Remove(task);
                _db.SaveChanges();
            }
            
        }

        public void Edit(int id, Tasks task)
        {
            Tasks tsk = _db.tasks.FirstOrDefault(x => x.Id == id);
            if (tsk != null)
            {
                foreach (Tasks temp in _db.tasks)
                {
                    temp.Name = task.Name;
                    temp.Description = task.Description;
                    temp.CreatedBy = task.CreatedBy;
                    temp.CreatedOn = task.CreatedOn;



                }

            }
            _db.tasks.Update(task);
           // throw new NotImplementedException();
        }

        public List<Tasks> GetTask()
        {
            return _db.tasks.ToList();
        }

        public Tasks GetTaskById(int id)
        {
            return _db.tasks.FirstOrDefault(x => x.Id == id);
        }
    }
}
