﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TaskApi.ITaskRepository;
using TaskApi.Models;

namespace TaskApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubTaskController : ControllerBase
    {
        // Add required dependencies here
        //public async Task<IActionResult> GetAllSubtasks()
        //{

        //}

        ISubTask _subTask;
        public SubTaskController(ISubTask subTask)

        {
            _subTask = subTask;
        }
        [HttpPost]

        public void Post(SubTask subtask)
        {
            _subTask.Create(subtask);

        }
        [HttpGet("{id}")]
        public List<SubTask> Get(int id)
        {
            return _subTask.GetSubTaskById(id);
        }


    }
}
