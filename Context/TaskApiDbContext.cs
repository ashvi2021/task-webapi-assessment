﻿using Microsoft.EntityFrameworkCore;
using TaskApi.Models;

namespace TaskApi.Context
{
    public class TaskApiDbContext : DbContext
    {
        public TaskApiDbContext ()
       {

       }
        public TaskApiDbContext(DbContextOptions<TaskApiDbContext> options) : base(options)
        {


        }
        public DbSet<SubTask> subTasks { get; set; }
        public DbSet<Tasks> tasks { get; set; }

        public DbSet<User> Users { get; set; }
        


    }
}
