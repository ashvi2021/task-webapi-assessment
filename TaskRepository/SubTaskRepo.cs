﻿using TaskApi.Context;
using TaskApi.ITaskRepository;
using TaskApi.Models;

namespace TaskApi.TaskRepository
{
    public class SubTaskRepo : ISubTask
    {
        TaskApiDbContext _db;
        public SubTaskRepo(TaskApiDbContext db)
        {
            _db = db;
        }
        public void Create(SubTask subtask)
        {
            _db.Add(subtask);
            _db.SaveChanges();
            
        }

        public List<SubTask> GetSubTask()
        {
            return _db.subTasks.ToList();
        }

        public List<SubTask> GetSubTaskById(int id)
        {
            return _db.subTasks.Where(x => x.TasksId == id).ToList();
        }
    }
}
