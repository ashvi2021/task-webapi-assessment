﻿using TaskApi.Models;

namespace TaskApi.ITaskRepository
{
    public interface ISubTask
    {
        List<SubTask> GetSubTask();
        public void Create(SubTask subtask);
        public List<SubTask> GetSubTaskById(int id);
    }
}
